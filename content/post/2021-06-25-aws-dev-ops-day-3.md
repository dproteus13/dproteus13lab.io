---
title: AWS Dev Ops Training - Day 3
date: 2021-06-25
tags: ["training-notes", "aws-devops"]
---

Module 9 - Automated Testing
----------------------------

One of the points made about the importance of replicating the production
environment for the purposes of testing, is to **empower experimentation**.  I
think that phrasing seems particularly good, so I wanted to write it down to use
later.

We had some discussion about the example pipeline shown on page 430 of the
Student Resource.  It seems like it is maybe a matter of my initial
interpretation of the diagram, *but* it still might be an interpretation worth
thinking more about in the future.  In the provided diagram, they show Unit
Tests and Static Code Analysis as being run in Development, and they don't list
them under Build (see table below).  IFF (if and only if) we could ensure that
Unit Tests and SCA were run during development, it _might_ be an opportunity to
optimize / reduce resource consumption.

| Development | Build |
|-------------|-------|
| Unit Tests | Integration Testing |
| Static Code Analysis | Component Testing |
| | Regression Testing |

Notionally, we'd have to have some frank discussions about this if we wanted to
pursue it, but some ideas include:

1. Pre-Commit Hooks
1. IDE Configurations (though I'm extra wary of this)
1. Pre-Receive Hooks on the server, maybe?
1. Leveraging results of Branch Builds on the following Pull Request builds,
   etc.?
   * This might be the _most_ promising option, but would frankly require
     some more thought and development...

Netflix, as one of the earliest large-scale adopters, _has_ developed a large
number of custom tools to manage and test these types of systems.  They refer to
that collection as their Simian Army, and this includes tools like
[Chaos Monkey](https://netflix.github.io/chaosmonkey/).

Performance Testing can leverage BlazeMeter (JMeter?), not sure if this is
limited to specific languages / interfaces, though?  Instructor also did say
there is a visualization that includes color-coding each container in the path
of traffic.

There is a diagram showing hybrid-cloud leveraging cloud for testing on page 447
of the Student Resource book.

Module 10 - Security Automation
-------------------------------

Might be worth knowing at some point, Amazon has a standard resource notation:

    "arn:aws:codepipeline:us-west-2:111222333444:MyFirstPipeline"

1. Format (here, Amazon Resource Notation: `arn`)
1. Provider (here: `aws`)
1. Service (here: `codepipeline`)
1. Region (here: `us-west-2`)
1. Account (here: `111222333444`)
1. Resource (here: `MyFirstPipeline`)

AWS STS (Security Token Service) seems like there must be an equivalent in
OpenStack that we should look into?  Or ideally a generic solution?

I wonder if they offer a Security-focused version of this training, that would
be shorter but that we could possibly get our whole Security Team to be sent
to? Instructor said there is a three-day training focused on Security that he
also teaches, but that sounds more focused on teams actually integrating with
AWS directly.  It doesn't sound like they have a course that would be
necessarily applicable to our On-Prem Enclave Security teams, unfortunately.

Apparently there is a Cloud Adoption Framework that's been developed by AWS, and
sounds like it might be a good read?  I'd guess that others in our company have
likely already read it, but I should read it too...

Module 11 - Configuration Management
------------------------------------

Here, Configuration Management is meant in the sense of Ansible control of
systems.  AWS, though, has their own tooling and also offers hosted Chef and
Puppet services, not Ansible.

Module 12 - Obervability
------------------------

Observability is more than just monitoring, it's turning data into information.

AWS X-Ray Service Graph (page 588) shows a great picture of the performance
monitoring type of view that I'd taken a note about earlier.

Instructor also mentioned a service called DataDog, which might be work looking
at, for monitoring and visualization.

Lab 5 - Performing blue/green deployments with CI/CD pipelines and Amazon Elastic Container Service
---------------------------------------------------------------------------------------------------

I spent a fair bit of time playing around with things during this lab.  It has a
lot of the piece-parts that I would want to use for experiments, so when I get a
crazy idea and want to try it out, this is probably the lab to use...

