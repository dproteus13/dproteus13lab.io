---
title: AWS Dev Ops Training - Day 1
date: 2021-06-23
tags: ["training-notes", "aws-devops"]
---

I had the good fortune to be sent, by my work, to a AWS Dev Ops Training class
provided by ExitCertified.  It is a three day class covering everything from a
DevOps overview through CICD Pipelines, Serverless, Deployment Strategies,
Security Automation, etc.  Labs will be worked in separate git repos, but I may
put links here for completeness.

One thing that I feel is worth highlighting, is that I was unable to use my work
laptop to perform the pre-class checks.  Specifically, I could not `ssh` to the
IP Address that was provided, presumably because of work's proxy / network
protection configuration.  As a result, I used my personal laptop for this
class, in order to make sure that I didn't run into tons of issues even trying
to start the lab assignments, etc.

My initial take on the course, based on Module 0 (Course Overview), is that
this is going to be a lot more applicable to me than I'd feared from the Course
Description when I first signed up.  That is, I'd thought it would be somewhat
applicable, but not terribly.  From the Course Overview, though, and the
instructor's promise to try to translate from AWS to OpenStack for me, has me
thinking that this is actually going to be a great match.

Module 1 - What is DevOps?
--------------------------

Amazon itself, while often considered a Unicorn, did actually start out
developing large monolithic systems.  They did transition, out of necessity to
be able to keep up with smaller competitors, from a large hierarchy to smaller,
self-contained teams.

The instructor specifically highlighted that *every single commit* gets build,
tested, and results in feedback to the developer.  A part of breaking things
down into smaller services is so that "Everyone gets to innovate at their own
pace."

Module 2 - Infrastructure Automation
------------------------------------

Cloud Formations supports parameterization in our own configs, which also
supports list _or_ regex-style validation.  While it isn't stated explicitly,
the instructor shared that he _thinks_ these are Perl-style regex's.

Because AMIs are specific to a given region, Mappings are commonly used to make
`region-id` to `ami-id` parameters.

AWS provides some cloud-utilities that are very useful, might not be included
with our own custom VMs?  Great example of one to use from that is
`/opt/aws/tools/cfn-signal` (double-check this, he went to another screen before
I got it all typed up) in order to send "ready" signal to Cloud Formations.  For
example, after running some custom provisioning, to let Cloud Formations know
that the provisioning is complete, etc.

Specific tools that the instructor highlighted as being useful:

* `cfn-init`
* `cfn-signal`
* `cfn-get-metadata`
* `cfn-hup`
* `cloud-init` (or `EC2Launch` on Windows :sick:)

Instructor gave a note that Cloud Formations tries to parallelize resource
creation, anecdotally seems to do so in top-down order.  In order to handle
dependencies, be sure to populate the `DependsOn` entry as needed.

While the instructor was discussing the need to enumerate dependencies, I
offered "there's no magic, you have to tell it."  He liked that "no magic" quote
and I really liked how he improved upon it:

| "No magic here, all automation.  Lightens the load on my fingers, not my brain."

There's a notional Microservice Architecture on page 59 of the Student Guide.

There is some support in Cloud Formation to do things like update / maintain
active resources.  That said, the instructor shared that his personal opinion is
that Cloud Formations isn't necessarily the best tool for the maintenance type
of tasking.  He agreed Ansible is a good solution for that, but he also
mentioned there are AWS-specific offerings that we will touch on later.

Cloud Formation has its own Service Catalog, which is meant for users (us) to
define / offer templates to users in their organization in order to simplify
self-service.

In Cloud Formation policies, Deny always takes precedence.  If there is **no**
policy provided at all, though, the default is to allow everything.  There is
also a specific `DeletionPolicy` option for certain resource types, which
supports a `Snapshot` policy to effectively back things up before deleting them.

Another great feature of CloudFormations is to support "Drift Detection," that
is to determine if some resource's configuration has drifted from the template
(for example, by someone logging in and installing something manually, etc.).

Module 3 - AWS Toolsets
-----------------------

The AWS CLI `text` output format is a terse (more machine-friends) output, vs
`table` which is formatted for human readability.  Might generally prefer the
`json` or `yaml` output anyway, _but_ there are times `text` might be easiest
for scripting, etc.

It's worth noting that the `ec2` service-group in the AWS CLI includes not
_just_ the EC2 controls, but also a lot of the networking, etc. types of things
that are not strictly from the EC2 Console but are relevant to building nodes
and making them usable.

For automation / scripting purposes, most service-groups support a `wait`
command to periodically check for certain conditions on the cloud.  It will
**not** wait forever, it has a maximum delay (instructor thinks it's 60 minutes
off the top of his head).

AWS SAM (Serverless Application Model) has its own CLI, and ends up using Cloud
Formation under the hood.  For SAM development, they actually have a local
development environment that is basically a local container in which to execute
the Lambda code.

For developing in the cloud, Amazon offers Cloud9, which is a collaborate IDE
with simultaneous editing options (similar to Google Docs or Confluence, showing
where each developer is), and chat, etc.  Interestingly, Cloud9 appears to
include out-of-the-box support for C/C++ in addition to some of the more
expected languages.  Cloud9, from a quick search on Docker Hub, looks like it
is available to be self-hosted.

AWS CodeStar is also an offering for overall project management, including
Dashboards, IDE, CICD Pipelines.  This might be something worth looking to see
if we can run on-prem, or at the very least it might be good inspiration for a
self-hosted solution.

Lab 1
-----

GitLab Project: https://gitlab.com/dproteus13/aws-devops-training-lab1

Using qwiklabs provided environment and accounts, including a Cloud9 IDE in the
browser.

Random observations from during the lab:

* The CLI requires `file://` to access local files... This feels awkward, but I
  tested and it *is* required :(
* The drift-detection capability is a huge item to bring to the attention of
  some of the folks who need more convincing.  Being able to see HOW resources
  have changed is a huge capability for stability and reliability.
* ChangeSets look to be a good way for basically previewing and reviewing how
  something will change?  It seems like this should be able to be integrated
  with the IaC SCM a little better, though?

Module 4 - CI/CD With Development Tools
---------------------------------------

Offer a full CICD environment which must be really nice for teams starting from
scratch.  That said, it's worth looking into how we might be able to learn from
what each of their tools does, even if we can't use them...  And there might be
some options to use them in pockets?

