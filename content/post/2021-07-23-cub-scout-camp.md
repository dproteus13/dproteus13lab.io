---
title: Cub Scout Camp 2021
date: 2021-07-23
tags: ["scouts"]
---

I will try to collect more of our experiences here shortly, including links to
the album of pictures that we got.  However, first and foremost I wanted to
capture my thoughts / suggestions for improvement next year, before I lost any
of them...

Suggestions for Improvement
---------------------------

1. Include the full list of dates and times for **everything** up front, on the
   sign up forms:
   * Adult Trainings
   * Cub Check-In
   * Camp days and times
   * Campfire Ceremony
   * Camp Cleanup
1. Maybe schedule things so that we can have a separate clean-up day the day
   after.  Expect a different mix of attendance, _but_ the younger cubs cannot
   hang any later on the last day.
1. The last day especially (but in general) try to schedule the younger dens to
   not trek back-and-forth across the camp a bunch of times.  Their legs are
   shorter, and they lost out on some of the station time in addition to just
   being completely wiped out on the last day.
1. On the last day, consider eliminating something (maybe Den Time) and having a
   Pizza Party or a BBQ or some way for the cubs to get extra food before
   closing ceremonies.  Worst case, include extra snacks, maybe handed out after
   closing ceremonies?  We ran late starting closing ceremonies, but we'd
   expected a 30 minute break to go grab dinner, so we ended up being a little
   late to Campfire.
1. It would be great to provide every Den Walker (ideally, but at least every
   Den) with a little "Essentials Booklet":
   * Fun Facts
   * Trivia Questions
   * Riddles
   * Chants
   * Songs
   * Other ideas for keeping the cubs entertained during any lull, etc.
   * Emergency phone numbers and procedures (I think these were already in Den
     Bags?)
1. One of the cubs in my den was asking the other cubs in the den for their
   phone numbers.  It might be nice to include the option on the Sign Ups to be
   included in a Den Directory and then give each scout their Den Directory so
   they can contact the new friends they've made (and help Parents have a bit
   more control over that).
1. For the Adult Volunteers, it was tough to come early or stay late because we
   didn't necessarily want to subject our cubs to extra time sitting around.
   Maybe consider shifting the Staff Meeting to being a Zoom call in the
   evening?  Also might help if there are extra special activities run by the
   Den Chiefs for those cubs before and after camp?  OR, since they're so wiped
   out by camp already, maybe set up a movie screen for them and they could just
   relax and watch something?  Even if they just watch their NetSmartz videos
   for the upcoming year's Cyber Chip, it would be something to keep them
   entertained but also unwind a bit.
1. Some of the Den Chiefs were phenomenal, and it would have been nice to do a
   little something extra to recognize them.  Could we maybe ask the Den Walkers
   and Camp Staff to submit suggestions for Yearbook-Style awards to be
   presented to the Den Chiefs during the Campfire Ceremonies?  "Most likely to
   be a Song Writer," "Most likely to become Camp Director," "Most likely to
   become a Drill Instructor," etc.  Or maybe we have a list of awards and ask
   the Den Walkers to vote?  Maybe give Den Walkers (on Day 1) a sheet to record
   a compliment / observation about each Den Chief throughout the week?
1. Recommend re-instituting the requirement that Packs provide 1 Adult Volunteer
   for every X Scouts they send.
1. Recommend treating camp for ticks prior to camp, and maybe even spraying the
   specific trails cubs are expected to take every morning with an over the
   counter treatment (possibly a fogger)?
