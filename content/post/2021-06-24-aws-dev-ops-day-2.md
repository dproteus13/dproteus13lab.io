---
title: AWS Dev Ops Training - Day 2
date: 2021-06-24
tags: ["training-notes", "aws-devops"]
---

Module 4 - CI/CD With Development Tools (cont'd)
------------------------------------------------

Jenkins Amazon EC2 Plugin (see slide on page 173) can automatically spin up /
down EC2 Build Agents.  This might be something that we could leverage on our
GovCloud-connected systems, similar to how we are using OpenStack on our
air-gapped system.

AWS CodeDeploy provides k8s-style deployment options to applications running on
servers.  It _does_ require a daemon agent to be running on the server(s), but
provides the ability to do a rolling deployment of updates, etc..  It also
supports deployment of Containers to AWS ECS, too, though I'm not sure what the
trade space between CodeDeploy->ECS vs EKS would be?  CodeDeploy AppSpec has a
**standard lifecycle**:

1. ApplicationStop
1. BeforeInstall
1. AfterInstall
1. ApplicationStart
1. ValidateService

It's important to note that the `ValidateService` step runs _after_ the full
deployment is complete, across all of the servers.  The instructor wasn't quite
sure if that grouping was controlled by the Deployment Strategy, or if it was
for the FULL set of servers.

* [x] If I finish the CodeDeploy lab with time to spare, pull down the completed
  lab and then try to experiment to see if I can iron out the specific timing /
  behaviour of the ValidateService lifecycle step, as compared to the Deployment
  Strategy.

This was more of a quick mention during the CodeDeploy notification discussion,
**but** there is an AWS SNS service that provide notification support?
Subscribers can select to receive notification via e-mail, SMS, etc...  I wonder
if we can possibly leverage this for better communication options?  I did a
quick GTS report, though, and it looks like GovCloud's version of SNS cannot
send SMS messages from US-East...  Might mean that we _can_ use it from US-West,
but I'm not 100% sure, and would need to look into it...


Lab 2 - Deploying an application to an EC2 fleet using AWS CodeDeploy
---------------------------------------------------------------------

The lab uses Windows AMIs running on `t2.large` EC2 nodes.  Since they were
large nodes, I didn't bother spinning up any more than was strictly required for
the lab, so I couldn't really play around with many Deployment Strategies.  That
said, I _did_ manage (after completing the normal lab) to do a quick experiment
related to the ValidateService question that came up during the module:

1. Complete Lab as normal / instructed
1. Modify `Updated-HeartBeat-App/HeartBeat-app/appspec.yml` to add a
   `ValidateService` step that runs a non-existent script `fail.ps1`
1. Deploy the updated (failing) package to the S3 bucket
1. Create the Deployment, but this time with `CodeDeployDefault.OneAtATime` for
   the `deployment-config-name`
1. Monitor the Deployment in the Browser
1. See one EC2 instance stepping through the Deployment Lifecycle
1. See that EC2 instance **Fail** at the `ValidateService` step
1. Confirmed that the *other* EC2 instance **Skipped** the deployment all
   together.

So while I _don't_ know if the `ValidateService` waits for all of the nodes in
the same stage / phase of deployment or not, I _can_ confirm that it **doesn't**
wait until the completion of all nodes.

Lab 3 - Automating code deployments using AWS CodePipeline
----------------------------------------------------------

This particular lab didn't quite meet my expectations.  There was not any actual
compilation of the code, just dropping a ZIP into an S3 bucket.

Module 5 - Introduction to Microservices
----------------------------------------

While discussing benefits of microservices, it was highlighted that _after the
transition_ the system can become easier to maintain and evolve, but that there
is a painful transition period.

Instructor took a brief side-bar to highlight some of the capabilities of AWS
Elastic Beanstalk (Orchestration Service).  It sounds like this adds a lot of
functionality, and it likely a good item to be looking for on-prem options that
are similar to it...

In general, the slide on page 252 has a good quick-reference of the *kinds* of
things that are needed for most Microservice systems.  The grouping of service
needs, and the AWS offering name, are both likely very useful for reference and
for searching for alternatives, etc.

Module 6 - DevOps and Containers
--------------------------------

Apparently there's a `.dockerignore` file that you can use to ignore files
during a `docker build` (ie. if you `COPY . /app` it won't copy those files).

As we go through this material, I find myself often running a GTS report for the
various AWS Service Offerings, to see if there's any FOSS / On-Prem options.
This has me realizing:

1. It might make a lot of sense to start by looking at the AWS Offerings /
   Catalog, and bouncing that off of our current plans, to see if there is
   anything that we're not yet thinking of, etc.  We should probably monitor that
   for updates over time, too.
1. I should look through the AWS Service Catalog to see if they offer a Service
   Catalog option?  Or, if not, if I can find any information about how AWS
   builds / manages their Service Catalog?

In AWS EKS, they offer a service called Parameter Store for free, which provides
secret storage as an option, too.

Module 7 - DevOps and Serverless Computing
------------------------------------------

Great at-a-glance overview of Serverless Offerings on page 316 of the Student
Guide / Resource book.

AWS Glue offering is meant for effectively ingesting various types of log files,
and distilling structure, filters, etc. which sounds a bit like it's doing some
of what I'd previously thought needed to be done by a Data Warehouse team?

AWS Lambda service runs small event-driven applications to handle a single
event, and then go down.  This is, under the hood, handled within Containers
(btw, AWS using Docker under the hood) running on AWS Fargate.  However, this is
potentially very powerful because if the applications are able to be made
event-driven, there are High Availability / Resiliency benefits, because the
container (and underlying Physical Hardware) doesn't necessarily need to be kept
up and operational, it's going to be spawned somewhere upon demand.  Said the
other way around, if a portion of physical compute goes offline unexpectedly,
future events are completely unaffected because they will just be executed on
physical resources that are still available (and there is not even an unexpected
delay in launching them, since they would have been launched anyway)

In AWS SQS (Simple Queue Service), messages need to be explicitly removed from
the queue once they've been successfully processed.  There is a "Visibility
Timeout" that prevents a future worker from getting that message while the
previous worker might still conceivably be working on that message.

AWS Lambda expects to receive applications as Zip files, which is interesting
but effectively sounds like they're kind of sort of like JARs or NARs, but just
didn't change the file extension to indicate they were special...

Module 8 - Deployment Strategies
--------------------------------

There's a little less applicability here, but I do think there's a lot worth
thinking about.  For the foreseeable future, the way we make Customer Deliveries
is likely to preclude us using these kinds of techniques.  _However_ there is a
potential to use these kinds of deployment techniques internally, both for our
development systems and also for our internal testing environments.  I need to
think through some of the nuances more, but notionally we could use these
techniques for some of our automated testing.  There _is_ a real trade to be
evaluated here, though, because if this differs greatly from our official
deployment mechanism, it _may_ not be as valuable of an investment.
**However**, if our customer is serious about moving in this direction, this may
be a way to drive in the direction they want in the nearer term.

Lab 4 - Deploying a serverless application using AWS Serverless Application Model (AWS SAM) and a CI/CD pipeline
--------------------------------

We agreed to work on this lab as homework tonight, in order to make sure we have
enough time for the material we have scheduled tomorrow.

This lab was very good, and included walking through a normal DevOps lifecycle
including committing code to a repository, and watching the traffic shifting of
a new deployment happen.
