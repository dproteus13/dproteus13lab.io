---
title: Some (Draft) Poems from my Notebook
date: 2022-04-12
tags: ["poetry", "rant"]
---

```
Don't need rhythm,
as I type.
Don't need rhythm,
to keep time.
Don't need rhythm,
don't need rhyme.
But they keep
my soul alive.
```

---

```
Type fast, gotta
get into flow.
Think deep, gotta
get into flow.
Work hard, gotta
get into flow.
It's addicting, yeah
all about that flow.
```

---

```
I put the hours in, and what do I get?
Meeting fatigue and more Inbox Debt.
```

---

```
Working all day,
can't get nothing done.
Hop from meeting to meeting,
collecting actions all day.
```
