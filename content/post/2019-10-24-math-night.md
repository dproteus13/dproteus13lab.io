---
title: Math Night
date: 2019-10-24
---

Eureka Math Parent Night
========================

Held at Thomas Paine Elementary, the Math Night is a program recommended by the
Eureka Math curriculum to get Parents exposed to the tools and techniques they
need to help their children excel with the curriculum.

Eureka Math is a new program for the district, but has been piloted for 4 years.
There is a change in education these days to focusing more on "how to do the
math" rather than solving many problems over and over again.  I like this, it's
reminiscent of Pitt's Honor's College's philosophy of teaching the theory.

Considered and piloted 4 programs at various schools over the last 4 years:

1. Eureka Math
1. envision Math 2.0
1. Investigations 3
1. Ready Math

Eureka Math was the program the teachers ultimately liked the best.  Johnson was
the first one to pilot this program, and the first month the teachers hated it
but then they loved it.  The same happened at Russel Knight when they piloted
Eureka Math.  Eureka Math also consistently gets the highest ratings from
http://edreports.org and is far and away the most used math curriculum across
the United States of America.

Eureka Math is perfectly aligned to NJ Standards.  Also provides a coherent /
cohesive story year over year. Eureka's "models" carry over year to year, so
the students are going to see the same strategies and models over and over
again.

The push for conceptual understanding ("why, not just how") is at least partly
so that we can teach more and more complex math at younger and younger ages.

In order to help build the concepts, and correct for difficulties in how the
English language diverges from math, we use something called "the Say Ten Way"
which means "one ten, two tens, three tens" for "ten, twenty, thirty."  There is
an activity to basically have the class count up and down in different ways
(including plain English) to reinforce the units, etc.

We went through a "Fluency Activity" that is called a "Sprint."  Basically,
solve as many problems as possible in 1 minute, then a little later do it again
with different problems.  Then measure the improvement and quickly show kids how
they've improved.  These are not collected or graded, they are for the students
to self-evaluate, and self-motivate.

Models / Strategies:

1. Number Bonds Break the numbers into groups to make 10s.  So for 998+337, you
   take 2 from the 337 to make the 998 turn into 1000, then just add the 335 to
   that and get 1335.  (If only we'd added 998+339, we could've ended up with
   1337!)
1. Tape Diagrams are kind of like drawing the problem out as "stamps", and can
   be used to solve fairly complicated problems without needing to resort to
   algebra.

Tips for Parent:

* Be positive!  If the students hear you grumbling "this is stupid, this isn't
how I did it" then they will think it's stupid (go figure!)
* There is an "EBoard" for Eureka Math Parent Help under each school's Parent
Resources page.
* The kids use the EBoards during school, so they should be able to help
navigate around there too.
* http://greatminds.org/signup has lots of Parent Resources
* Zearn is an online resource, which sounds like RazzKids but for Eureka
Math (link should be on the EBoard)
