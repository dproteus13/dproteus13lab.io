---
title: Favorite Quotes
subtitle: Bite-Sized Bits of Wisdom
tags: ["quotes"]
comments: false
---

1. 'Great Works and Great Folly, may be indistinguishable at the outset." --
   Mark Steltzer re. Curiosity Rover landing system
