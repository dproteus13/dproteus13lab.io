---
title: Avatar Gallery
subtitle: aka Nerd Badges
tags: ["gallery", "avatar"]
bigimg: [{src: "/img/triangle.jpg", desc: "Triangle"}, {src: "/img/sphere.jpg", desc: "Sphere"}, {src: "/img/hexagon.jpg", desc: "Hexagon"}]
comments: false
---

# Because every nerd needs their badges!

----

  {{< figure thumb="-thumb" src="/img/avatars/SEI-Architect-Official-Authorized-Seal.png" >}}
  {{< figure thumb="-thumb" src="/img/avatars/prosci-certified-change-practioner.png" >}}
  {{< figure thumb="-thumb" src="/img/avatars/bitmoji.jpg" >}}
  {{< figure thumb="-thumb" src="/img/avatars/data.gif" >}}
  {{< figure thumb="-thumb" src="/img/avatars/dilbert-coffee.png" >}}
  {{< figure thumb="-thumb" src="/img/avatars/dnd.jpg" >}}
  {{< figure thumb="-thumb" src="/img/avatars/initials.png" >}}
  {{< figure thumb="-thumb" src="/img/avatars/link.gif" >}}
  {{< figure thumb="-thumb" src="/img/avatars/linux-avatar.png" >}}
  {{< figure thumb="-thumb" src="/img/avatars/stick-figure-head-bang.gif" >}}
  {{< figure thumb="-thumb" src="/img/avatars/triforce.gif" >}}

