---
title: Favorite Gifs
subtitle: Me Memes, Me memes!
tags: ["gallery", "memes", "gif"]
bigimg: [{src: "/img/triangle.jpg", desc: "Triangle"}, {src: "/img/sphere.jpg", desc: "Sphere"}, {src: "/img/hexagon.jpg", desc: "Hexagon"}]
comments: false
---

# For quick access...

----

{{< figure thumb="-thumb" src="https://media.tenor.com/UXoHVryAgQUAAAAM/lord-of-the-rings-gandalf.gif">}}
{{< figure thumb="-thumb" src="https://media.tenor.com/F3fWX3TSonUAAAAM/kids-leave.gif">}}
{{< figure thumb="-thumb" src="https://c.tenor.com/omHtbO_Yl-MAAAAM/no-signal-static.gif">}}
{{< figure thumb="-thumb" src="https://c.tenor.com/JmY5d8M68v8AAAAM/witcher-sleep.gif">}}
{{< figure thumb="-thumb" src="https://media.tenor.com/EhYlvO0KUrMAAAAM/why-do-you-hate-me-scrubs.gif">}}
{{< figure thumb="-thumb" src="https://c.tenor.com/d96woq03MlIAAAAM/coffee.gif">}}
{{< figure thumb="-thumb" src="https://c.tenor.com/ZpskbnKLmaAAAAAM/love-that-journey-for-me-alexis.gif">}}
{{< figure thumb="-thumb" src="https://media.tenor.com/QjTxTGlu5qMAAAAM/star-wars-red-leader.gif">}}
{{< figure thumb="-thumb" src="https://c.tenor.com/bdF3Jc94jw4AAAAM/robert-downey-jr-iron-man.gif">}}
{{< figure thumb="-thumb" src="https://media.tenor.com/itSd_mLRHBUAAAAM/highfive-scrubs.gif">}}
