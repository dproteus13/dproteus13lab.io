---
title: About me
comments: false
---

# Passionate Engineer, Polymath, and Proud Dad

I am a proud father of two awesome kids:

- Arthur, my oldest, is extremely smart and about as well behaved as any kid I
have ever seen or heard of.  He is an even pickier eater than my wife or I ever
were, though!  Artie is interested in building things and designs clever
mechanisms, an aspiring engineer already, with a bent for artistry!
- Joseph, my youngest, is absolutely hilarious despite being a little difficult
whenever he can be.  Of course, Joey will eat anything he can get access to!  He
has always had a tenacious veracity for understanding the world around him, and
how it works.  Joey is also taking piano lessons, but is an amazing musician,
always singing or humming.

### source ~/joy.txt

I have not managed to read all the way through Marie Kondo's book, but I have
started and one thing that struck me was her search for Joy.  I have a hard time
getting rid of things, and part of that is that I find joy in fixing things, and
building new things out of old things.  Unfortunately, both of those require me
to accumulate broken "stuff," but I am hoping to better channel and control this
moving forward.

I also very much enjoy spending time in our yard, especially vegetable gardening
and processing firewood.  I find myself drawn to using hand tools and ancient
methods as a way of connecting with our planet and ecosystem, as well as the
satisfaction and one small way I can reduce my impact on our climate.  There is
not really a better workout that chopping firewood with an axe or maul!
